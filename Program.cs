﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChecksumContextMenuTool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string filename = "";
            if (args.Length > 0)
            {
                filename = args[0];

                if (!File.Exists(filename))
                {
                    MessageBox.Show("The system cannot find the file specified.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            } 
            else 
            {

                MessageBox.Show("No file argument specified.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ChecksumToolForm(filename));
        }
    }
}

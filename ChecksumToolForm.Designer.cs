﻿
namespace ChecksumContextMenuTool
{
    partial class ChecksumToolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.algorithmLabel = new System.Windows.Forms.Label();
            this.algorithm = new System.Windows.Forms.ComboBox();
            this.calcButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.actualHashInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.expHash = new System.Windows.Forms.TextBox();
            this.pasteBtn = new System.Windows.Forms.Button();
            this.openBtn = new System.Windows.Forms.Button();
            this.verifyBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.filenameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // algorithmLabel
            // 
            this.algorithmLabel.AutoSize = true;
            this.algorithmLabel.Location = new System.Drawing.Point(12, 35);
            this.algorithmLabel.Name = "algorithmLabel";
            this.algorithmLabel.Size = new System.Drawing.Size(71, 17);
            this.algorithmLabel.TabIndex = 0;
            this.algorithmLabel.Text = "Algorithm:";
            // 
            // algorithm
            // 
            this.algorithm.FormattingEnabled = true;
            this.algorithm.Items.AddRange(new object[] {
            "SHA-1",
            "SHA-256",
            "SHA-512",
            "MD5",
            "BSD (Unix)",
            "SYSV (Unix)",
            "CRC-16",
            "CRC-32",
            "CRC-64"});
            this.algorithm.Location = new System.Drawing.Point(89, 32);
            this.algorithm.Name = "algorithm";
            this.algorithm.Size = new System.Drawing.Size(281, 24);
            this.algorithm.TabIndex = 1;
            // 
            // calcButton
            // 
            this.calcButton.Location = new System.Drawing.Point(393, 35);
            this.calcButton.Name = "calcButton";
            this.calcButton.Size = new System.Drawing.Size(183, 23);
            this.calcButton.TabIndex = 2;
            this.calcButton.Text = "Calculate";
            this.calcButton.UseVisualStyleBackColor = true;
            this.calcButton.Click += new System.EventHandler(this.calcBtn_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(582, 35);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(183, 23);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Actual Hash:";
            // 
            // actualHashInput
            // 
            this.actualHashInput.Cursor = System.Windows.Forms.Cursors.No;
            this.actualHashInput.Location = new System.Drawing.Point(132, 64);
            this.actualHashInput.Name = "actualHashInput";
            this.actualHashInput.ReadOnly = true;
            this.actualHashInput.Size = new System.Drawing.Size(633, 22);
            this.actualHashInput.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Expected Hash:";
            // 
            // expHash
            // 
            this.expHash.Location = new System.Drawing.Point(132, 93);
            this.expHash.Name = "expHash";
            this.expHash.Size = new System.Drawing.Size(633, 22);
            this.expHash.TabIndex = 7;
            // 
            // pasteBtn
            // 
            this.pasteBtn.Location = new System.Drawing.Point(15, 122);
            this.pasteBtn.Name = "pasteBtn";
            this.pasteBtn.Size = new System.Drawing.Size(226, 23);
            this.pasteBtn.TabIndex = 8;
            this.pasteBtn.Text = "Paste";
            this.pasteBtn.UseVisualStyleBackColor = true;
            // 
            // openBtn
            // 
            this.openBtn.Location = new System.Drawing.Point(282, 122);
            this.openBtn.Name = "openBtn";
            this.openBtn.Size = new System.Drawing.Size(226, 23);
            this.openBtn.TabIndex = 9;
            this.openBtn.Text = "Open...";
            this.openBtn.UseVisualStyleBackColor = true;
            // 
            // verifyBtn
            // 
            this.verifyBtn.Location = new System.Drawing.Point(539, 122);
            this.verifyBtn.Name = "verifyBtn";
            this.verifyBtn.Size = new System.Drawing.Size(226, 23);
            this.verifyBtn.TabIndex = 10;
            this.verifyBtn.Text = "Verify";
            this.verifyBtn.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Selected File:";
            // 
            // filenameLabel
            // 
            this.filenameLabel.AutoSize = true;
            this.filenameLabel.Location = new System.Drawing.Point(112, 9);
            this.filenameLabel.Name = "filenameLabel";
            this.filenameLabel.Size = new System.Drawing.Size(0, 17);
            this.filenameLabel.TabIndex = 12;
            // 
            // ChecksumToolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 170);
            this.Controls.Add(this.filenameLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.verifyBtn);
            this.Controls.Add(this.openBtn);
            this.Controls.Add(this.pasteBtn);
            this.Controls.Add(this.expHash);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.actualHashInput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.calcButton);
            this.Controls.Add(this.algorithm);
            this.Controls.Add(this.algorithmLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ChecksumToolForm";
            this.Text = "Checksum Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label algorithmLabel;
        private System.Windows.Forms.ComboBox algorithm;
        private System.Windows.Forms.Button calcButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox actualHashInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox expHash;
        private System.Windows.Forms.Button pasteBtn;
        private System.Windows.Forms.Button openBtn;
        private System.Windows.Forms.Button verifyBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label filenameLabel;
    }
}

